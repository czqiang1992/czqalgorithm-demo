//
//  MainApi.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/24.
//

import Foundation

class MainApi: NSObject{
    struct Home {
        static let homeInfo = MainApiItem("github.com", description: "商品介绍", method: "POST")
    }
}

struct MainApiItem: ApiItemProtocol{
    var pathURL: String
    
    var url: String{
        return domain + pathURL
    }
    
    var method: String
    
    var description: String
    
    var domain: String = "Main===="
    
    init(_ urlPath: String,
         description: String,
         method: String) {
        self.pathURL = urlPath
        self.description = description
        self.method = method
    }
}
