//
//  ProductApi.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/24.
//

import Foundation

class ProductApi{
    static let domain = "jiangxisheng"
    
    struct Product {
        static let productList = ProductApiItem("baidu.com", description: "商品介绍", method: "GET")
    }
    
    static func loadRequest(item: ApiItem) -> String {
        return Self.domain + item.url
    }
    
    var domain: String = "shi a shia"
}

struct ProductApiItem: ApiItemProtocol{
    var pathURL: String
    
    var method: String
    
    var description: String
    
    var domain: String = "Product===="
    
    init(_ urlPath: String,
         description: String,
         method: String) {
        self.pathURL = urlPath
        self.description = description
        self.method = method
    }
}
