//
//  ApiItem.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/24.
//

import Foundation

struct ApiItem {
    var url: String
    var method: String
    var description: String
    
    var domain: String = "chenzhiqiang"
    
//    init(domain: String) {
//        self.domain = domain
//    }
    init(url: String, method: String, des: String) {
        self.url = url
        self.method = method
        self.description = des
    }
 }
