//
//  ApiItemProtocol.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/25.
//

import Foundation

protocol ApiItemProtocol {
    var pathURL: String { get }
    var method: String { get }
    var description: String { get }
    
    var domain: String { get }
    
    func loadRequestURL() -> String
}

extension ApiItemProtocol{
    func loadRequestURL() -> String{
        return domain + pathURL
    }
}
