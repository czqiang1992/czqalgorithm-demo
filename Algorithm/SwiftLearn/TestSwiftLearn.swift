//
//  TestSwiftLearn.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/10/14.
//

import Foundation

struct CHEN<Base>{
    var base: Base
    
    init(base: Base) {
        self.base = base
    }
}

extension String {
    var chen: CHEN<String>{
        return CHEN(base: self)
    }
    
    static var chen: CHEN<String>.Type{
        return CHEN<String>.self
    }
}

extension CHEN where Base == String{
    func doTestChar(){
        print("haha")
    }
    
    static func fly(){
        
    }
}

class buildClass{
    let str: String = "china"

    func doChenTest(){
        str.chen.doTestChar()
        
        let person = TestPerson()
        person.chen.goFun()
        
        String.chen.fly()
    }
}

class TestPerson{}
extension TestPerson{
    var chen: CHEN<TestPerson>{
        return CHEN(base: self)
    }
}

extension CHEN where Base: TestPerson{
    func goFun(){
        
    }
}
