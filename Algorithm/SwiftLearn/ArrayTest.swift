//
//  ArrayTest.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/10/15.
//

import Foundation

protocol ArrayEqualProtol {
    
}

extension Array: ArrayEqualProtol{
}
extension NSArray: ArrayEqualProtol{
}

class ArrayTest {
    func isArray(item: Any) -> Bool{
        return item is Array<Any>
    }

//    func isArrayType(type: Any.Type) -> Bool{
//        return type is Array<Any>.Type
//    }
    func isArrayType(type: Any.Type) -> Bool{
        return type is ArrayEqualProtol.Type
    }
    
    init() {
//        let b = ["1","23"]
//        let c = NSMutableArray()
//        let d = ""
        
//        print(isArray(item: b))
//        print(isArray(item: c))
//        print(isArray(item: d))
        
        let a = Array<Int>.self
        let b = Array<Any>.self
        let c = NSMutableArray.self
        let d = String.self
        print(isArrayType(type: a))
        print(isArrayType(type: b))
        print(isArrayType(type: c))
        print(isArrayType(type: d))
    }
}
