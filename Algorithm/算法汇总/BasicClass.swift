//
//  BasicClass.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/7/26.
//

import Foundation

protocol BasicProtocol {
    static func TestEasy()
    static func TestOptimize()
}

class BasicClass{
    class func TestEasy(){
        print("TestEasy")
    }
    
    class func TestOptimize(){
        
    }
}
