//
//  00Test.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/7/29.
//

/*
    写一个函数，输入 n ，求斐波那契（Fibonacci）数列的第 n 项（即 F(N)）。斐波那契数列的定义如下：

    F(0) = 0,   F(1) = 1
    F(N) = F(N - 1) + F(N - 2), 其中 N > 1.
    斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。

    答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
*/

import Foundation

class test: BasicProtocol {
    static func TestOptimize() {
        let num = TestOptimize(target: 5)
        print(num)
    }
    
    static func TestEasy() {
        let newCode = Solution_code_revrse.init().reverseCharacter(text: "tuoyaLotuAwohS/6101gnim/moc.buhtig//:sptth")
        print(newCode)
    }
    
    //累加解法 F(0) = 0, F(1) = 1 F(2) = 1 F(3) = 2
    static func TestOptimize(target: Int)-> Int {
        var num1 = 0, num2 = 1, sum = 0
        for _ in 0..<target {
            sum = (num1+num2)%1000000007
            num1 = num2
            num2 = sum
        }
        return num1
    }
    

}
