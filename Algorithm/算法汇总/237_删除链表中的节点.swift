//
//  237_删除链表中的节点.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/8/4.
/*
 示例 1：

 输入：head = [4,5,1,9], node = 5
 输出：[4,1,9]
 解释：给定你链表中值为 5 的第二个节点，那么在调用了你的函数之后，该链表应变为 4 -> 1 -> 9.
 示例 2：

 输入：head = [4,5,1,9], node = 1
 输出：[4,5,9]
 解释：给定你链表中值为 1 的第三个节点，那么在调用了你的函数之后，该链表应变为 4 -> 5 -> 9.
 
 提示：

 链表至少包含两个节点。
 链表中所有节点的值都是唯一的。
 给定的节点为非末尾节点并且一定是链表中的一个有效节点。
 不要从你的函数中返回任何结果。

 来源：力扣（LeetCode）
 链接：https://leetcode-cn.com/problems/delete-node-in-a-linked-list
 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */

/*
 思路：然后当前node的值为下个节点的值
 尔后当前node的next指向下下个节点
 */

import Foundation

class Solution_237 {
    func deleteNode(_ node: ListNode?) {
        if let curNode = node{
            curNode.val = curNode.next?.val ?? 0
            curNode.next = curNode.next?.next
        }
    }
}
