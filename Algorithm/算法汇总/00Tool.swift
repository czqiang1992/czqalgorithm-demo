//
//  00Tool.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/8/5.
//

import Foundation

class Solution_code_revrse {
    func reverseCharacter(text: String) -> String {
        guard text.count>0 else {
            return ""
        }
        var characterArray:[Character] = []
        for characterText in text {
            characterArray.append(characterText)
        }
        characterArray.reverse()
        
        var outputStr: String = ""
        for item in characterArray{
            outputStr = outputStr + String(item)
        }
        print("outputStr:\(outputStr)")
        return outputStr
    }
}
