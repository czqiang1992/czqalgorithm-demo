import UIKit

/*
 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。

 有效字符串需满足：

 左括号必须用相同类型的右括号闭合。
 左括号必须以正确的顺序闭合。
  

 示例 1：

 输入：s = "()"
 输出：true
 示例 2：

 输入：s = "()[]{}"
 输出：true
 示例 3：

 输入：s = "(]"
 输出：false
 示例 4：

 输入：s = "([)]"
 输出：false
 示例 5：

 输入：s = "{[]}"
 输出：true
 */

class Solution {
    func isAnotherValid(_ s: inout String) -> Bool {
        while (s.contains("[]")||s.contains("()")||s.contains("{}"))&&s.count>0 {
            s = s.replacingOccurrences(of: "()", with: "")
            s = s.replacingOccurrences(of: "[]", with: "")
            s = s.replacingOccurrences(of: "{}", with: "")
        }
        
        return s.count == 0
    }
    
    func isValid(_ s: String) -> Bool {
        let dic:[Character:Character] = [")":"(","]":"[","}":"{"]
        var dataArray:[Character] = []
                
        for char in s {
            if dic.values.contains(char) {
                dataArray.append(char)
            }else if dic.keys.contains(char){
                if let anotherChar = dic[char], let lastChar = dataArray.last{
                    if anotherChar == lastChar {
                        dataArray.removeLast()
                    }else{
                        return false
                    }
                }else{
                    return false
                }
            }else{
                return false
            }
        }
                                    
        return dataArray.count==0
    }
}

var s:String = "[{}{}]"
Solution().isAnotherValid(&s)
