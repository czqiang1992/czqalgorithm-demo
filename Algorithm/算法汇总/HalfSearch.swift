//
//  HalfSearch.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/22.
//

import Foundation

class HalfSearch{
    var dataArray = [3,5,7,8,9,12,16,20]
        
    func searchNumber(goalNumber: Int) -> Int?{
        var begin = 0
        var end = dataArray.count
        
        while begin<end {
            let middle = (begin + end) / 2
            let middleNumber = dataArray[middle]
            if middleNumber == goalNumber{
                return middle
            }else if middleNumber < goalNumber{
                begin = middle + 1
            }else{
                end = middle
            }
        }
        return nil
    }
}
