//
//  01TwoSum.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/7/26.
//

/*
 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。

 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

 你可以按任意顺序返回答案。
 
 输入：nums = [2,7,11,15], target = 9
 输出：[0,1]
 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 */

import Foundation

class TwoSum: BasicProtocol {
    static let numbersArray = [2,4,5,7,3,6,9]
    static let target = 11
    static var goalArray:[Int] = []
    static var resultArray:[[Int]] = []

    static func TestEasy() {
        print("override")
 
        for (index,item) in numbersArray.enumerated(){
            let goalItem = target - item
            for (anotherIndex,anotherItem) in numbersArray.enumerated(){
                if anotherItem == goalItem {
                    goalArray = [index,anotherIndex]
                    resultArray.append(goalArray)
                }
            }
        }
        ATLog("target:\(target) resultArray:\(resultArray)")
    }
    
    static func TestOptimize() {
        var dataDict:[Int:Int] = [:]
        print("override")
        for (index,item) in numbersArray.enumerated(){
            if let dataIndex = dataDict[item] {
                goalArray = [index,dataIndex]
                resultArray.append(goalArray)
            }else{
                let goal = target - item
                dataDict[goal] = index
            }
        }
        ATLog("target:\(target) resultArray:\(resultArray)")
    }
}
