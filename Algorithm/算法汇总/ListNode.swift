//
//  ListNode.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/8/4.
//

import Foundation

class ListNode {
    var val: Int
    var next: ListNode?
    
    init(val: Int) {
        self.val = val
    }
}
