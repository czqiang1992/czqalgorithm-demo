//
//  ViewController.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/7/26.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "算法集合"
        
        let easyTestBtn = UIButton(frame: CGRect(x: 20, y: 150, width: 100, height: 100))
        easyTestBtn.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        easyTestBtn.setTitle("算法测试", for: .normal)
        easyTestBtn.setTitleColor(UIColor.darkText, for: .normal)
        easyTestBtn.addTarget(self, action: #selector(clickTestAlgorithm), for: .touchUpInside)
        self.view.addSubview(easyTestBtn)
        
        let testOptimizeBtn = UIButton(frame: CGRect(x: 20, y: 280, width: 100, height: 100))
        testOptimizeBtn.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        testOptimizeBtn.setTitle("算法测试(优化)", for: .normal)
        testOptimizeBtn.setTitleColor(UIColor.darkText, for: .normal)
        testOptimizeBtn.addTarget(self, action: #selector(clickOptimizeTestAlgorithm), for: .touchUpInside)
        self.view.addSubview(testOptimizeBtn)
        
        let swiftLearnBtn = UIButton(frame: CGRect(x: 150, y: 280, width: 100, height: 100))
        swiftLearnBtn.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        swiftLearnBtn.setTitle("swift学习", for: .normal)
        swiftLearnBtn.setTitleColor(UIColor.darkText, for: .normal)
        swiftLearnBtn.addTarget(self, action: #selector(clickSwiftLearn), for: .touchUpInside)
        self.view.addSubview(swiftLearnBtn)
    }

    @objc func clickTestAlgorithm(){
        print("begin easy Test")
        
        test.TestEasy()
        
        print("finish easy Test")
    }
    
    @objc func clickOptimizeTestAlgorithm(){
        print("begin optimze Test")
        
        test.TestOptimize()
        
        print("finish optimze Test")
    }
    
    @objc func clickSwiftLearn(){
        ATLog("learn")
        let ctr = FunctionClassController()
        self.navigationController?.pushViewController(ctr, animated: true)
    }
}

