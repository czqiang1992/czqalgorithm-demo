import UIKit

var greeting = "Hello, playground"

let dataArray = [1,2,3,4]
print(dataArray.startIndex)
print(dataArray.endIndex)

struct PersonSize{
    var x = 0, y = 0
    
    static func + (num1: PersonSize, num2: PersonSize) -> PersonSize{
        return PersonSize(x: num1.x+num2.x, y: num1.y+num2.y)
    }
}

let person1 = PersonSize(x: 20, y: 10)
let person2 = PersonSize(x: 10, y: 30)

let person3 = person1+person2
print(person3)

var arr = [2,4,6,8]
print(arr.reduce([], { $0 + [$1 * 2]}))

var name: String? = "abc"

let newname = name ?? "" + "zhi"
print(newname)

let index = arr.first { item in
    item == 6
}


struct Student{
    var name: String
    var age: Int
}

let studentArray = [Student(name: "chen", age: 12),
                    Student(name: "zhi", age: 13),
                    Student(name: "qiang", age: 14)]

func loadStudentMethodOne(name: String) -> Student?{
    for student in studentArray {
        if student.name == name {
            return student
        }
    }
    return nil
}

func loadStudentMethodTwo(name: String) -> Student?{
    let index = studentArray.firstIndex { student in
        student.name == name
    }
    return index != nil ? studentArray[index!] : nil
}

func loadStudentMethodThree(name: String) -> Student?{
    studentArray.firstIndex { student in
       student.name == name
    }.map { index in
        return studentArray[index]
    }
}


let zhiStu = loadStudentMethodOne(name: "zhi")
ATLog(zhiStu)

let zhistu2 = loadStudentMethodTwo(name: "zhi")
ATLog(zhistu2)

let zhistu3 = loadStudentMethodTwo(name: "zhi")
ATLog(zhistu3)
