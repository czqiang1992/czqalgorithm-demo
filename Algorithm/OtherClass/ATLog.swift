//
//  ATLog.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/7/26.
//

import Foundation

func ATDetailLog<T>(_ message: T,
                file: String = #file,
                method: String = #function,
                line: Int = #line) {
    #if DEBUG
    let date = NSDate()
    let timeFormatter = DateFormatter()
    timeFormatter.dateFormat = "HH:mm:ss"
    let nowTimer = timeFormatter.string(from: date as Date) as String
    
    print("""
    <************************ATLogDetail
    className:\((file as NSString).lastPathComponent)[\(line)]
    methodName:\(method)
    message:\(message)
    time:\(nowTimer)
    ATLogDetail************************>
    """)
    #endif
}

func ATLog<T>(_ message: T){
    print("********** \(message) **********")
}

// MARK: - other
func ATCustomLog<T> (message: T, error: Error? = nil, funcName: String = #function) {
    #if DEBUG
    if (error != nil) {
        print(" [TPNS] \(funcName)===>message: \(message)  error: \(error.debugDescription)")
    } else {
        print(" [TPNS] \(funcName)===>message: \(message)")
    }
    #endif
}


