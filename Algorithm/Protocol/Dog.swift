//
//  Dog.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/23.
//

import Foundation

class Dog: Animal, AnimalProtocol{
    var name: String{
        return "小狗"
    }
    
    func run() {
        print("\(name) is run")
    }
    
}
