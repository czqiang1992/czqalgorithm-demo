//
//  Bird.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/23.
//

import Foundation

class Bird: Animal , AnimalProtocol{
    var name: String{
        return "小鸟"
    }
    
    func run() {
        print("\(name) is run")
    }
    
}
