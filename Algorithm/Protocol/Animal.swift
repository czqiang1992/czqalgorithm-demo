//
//  Animal.swift
//  Algorithm
//
//  Created by 陈志强 on 2021/11/23.
//

import Foundation

protocol AnimalProtocol {
    var name: String{
        get
    }
    
    func run()
}

class Animal{
    
}
